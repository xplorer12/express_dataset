var { Pool } = require('pg');
var config = require("./config");

var pool = new Pool({ connectionString: config.database });

pool.connect(err => {
	if (err) {
		return console.error('Error acquiring client', err.stack);
	}
});

module.exports = {
   	query: async (text, values, callback) => {
		return await pool.query(text, values);
  	},
}