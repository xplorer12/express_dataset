module.exports = {
    'port': process.env.PORT || 9500,
    'database' : "postgresql://postgres:delta@localhost:5432/express_dataset",

    apps : [{
        name: "express-dataset-api",
        script: "app.js",
        watch: true,
        env: {
        	"NODE_ENV": "development",
        },
        env_production: {
        	"NODE_ENV": "production"
        }
    }]
}