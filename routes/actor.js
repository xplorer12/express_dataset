var express = require('express');
var router = express.Router();
var actorsController = require("../controllers/actors");

// Routes related to actor.

router.route("/")
    .put(actorsController.updateActor)
    .get(actorsController.getAllActors)

router.get("/streak", actorsController.getStreak);

module.exports = router;