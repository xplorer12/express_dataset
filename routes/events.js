var express = require('express');
var router = express.Router();
var eventsController = require("../controllers/events");

// Routes related to event

router.route('/')
    .post(eventsController.addEvent)
    .get(eventsController.getAllEvents);

router.get("/actor/:actorID", eventsController.getByActor);

module.exports = router;