DROP TABLE IF EXISTS actor CASCADE;
DROP TABLE IF EXISTS repo CASCADE;
DROP TABLE IF EXISTS event CASCADE;

CREATE TABLE actor (
	id character varying(255),
	PRIMARY KEY(id),
	login character varying(255),
	avatar_url character varying(255)
);

CREATE TABLE repo (
	id character varying(255),
	PRIMARY KEY(id),
	name character varying(255),
	url character varying(255)
);

CREATE TABLE event (
	id character varying(255) UNIQUE,
	PRIMARY KEY(id),
	type character varying(255),
	actor_id character varying(255),
	repo_id character varying(255),
	created_at timestamp without time zone,
	CONSTRAINT fk_actor
    	FOREIGN KEY(actor_id) 
	  	REFERENCES actor(id),
	CONSTRAINT fk_repo
    	FOREIGN KEY(repo_id) 
	  	REFERENCES repo(id)
)