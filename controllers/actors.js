var db = require("../db");

var getAllActors = async (req, res) => {
	try {
		const query = `SELECT act.id, act.login, act.avatar_url FROM actor act INNER JOIN event evt ON (evt.actor_id = act.id) GROUP BY act.id, evt.created_at ORDER BY COUNT(*) DESC, evt.created_at DESC, act.login ASC`;
	
		const result = await db.query(query);
	
		return res.status(200).json(result.rows);

	} catch (error) {
		return res.status(500).json({message: error.message});
	}
};

var updateActor = async (req, res) => {
	try {
		
		let update_query = 'UPDATE actor SET avatar_url = $1 RETURNING *';
        let update_values = [req.body.avatar_url];

		let actor = await db.query('SELECT * from actor act WHERE act.id = $1 AND act.login = $2 ', [req.body.id, req.body.login]);
		if(!actor.rowCount) return res.status(404).json({message: "Actor does not exist."});

		if(Object.keys(req.body).length !== 3) return res.status(400).json({message: "Only avatar url is to be updated."});

		let update_actor = await db.query(update_query, update_values);
		if(!update_actor) return res.status(500).json({message: "Unable to update actor's avatar url."});

		return res.status(200).json({message: "Update was successful."});
	} catch (error) {
		return res.status(500).json({message: error.message});
	}
};

var getStreak = async (req, res) => {
	try {
		const query = `SELECT DISTINCT act.id, act.login, act.avatar_url, evt.created_at, COUNT(*) FROM actor act INNER JOIN event evt ON evt.actor_id = act.id GROUP BY evt.created_at, act.id HAVING COUNT(*) >= 1 ORDER BY COUNT(*) DESC, evt.created_at DESC, act.login ASC`;
	
		const result = await db.query(query);
	
		const data = result.rows.map((res)=>{
			delete res.streak;
			return res;
		});

		var final_result = [];

		data.forEach(item => {
			var obj = {};

			obj["id"] = item.id;
			obj["login"] = item.login;
			obj["avatar_url"] = item.avatar_url;

			final_result.push(obj);
		});
	
		res.status(200).json(final_result);
	  } catch (error) {
		return res.status(500).json({message: error.message});
	  }
};

module.exports = {
	updateActor: updateActor,
	getAllActors: getAllActors,
	getStreak: getStreak
};