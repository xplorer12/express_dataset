var db = require("../db");
const utilities = require("../utils/formatJsonData");

var getAllEvents = async (req, res) => {

	let query = 'SELECT *, evt.id FROM event evt INNER JOIN actor act ON act.id = evt.actor_id INNER JOIN repo rep ON rep.id = evt.repo_id ORDER BY evt.id';

	let events = await db.query(query);

	events = utilities.formatJsonData(events.rows);

	return res.status(200).json(events);
};

var addEvent = async (req, res) => {

	if(!req.body.id) return res.status(400).json({message: "event-id-required"});
	if(!req.body.type) return res.status(400).json({message: "event-type-required"});
	if(!req.body.repo) return res.status(400).json({message: "repo-required"});
	if(!req.body.actor) return res.status(400).json({message: "actor-required"});

	var created_at = new Date();

	var actor_query = 'INSERT INTO actor(id, login, avatar_url) VALUES($1, $2, $3) RETURNING *';
	var actor_values = [req.body.actor.id, req.body.actor.login, req.body.actor.avatar_url];

	var repo_query = 'INSERT INTO repo(id, name, url) VALUES($1, $2, $3) RETURNING *';
	var repo_values = [req.body.repo.id, req.body.repo.name, req.body.repo.url];

	var initial_query = 'SELECT * FROM event WHERE ID = $1';
	var initial_query_value = [req.body.id];

	try {

		var id_exists = await db.query(initial_query, initial_query_value);
		if(id_exists.rowCount === 1) return res.status(500).json({});

		var create_actor = await db.query(actor_query, actor_values);
		if(create_actor.rowCount !== 1) return res.status(500).json({});

		var create_repo = await db.query(repo_query, repo_values);
		if(create_repo.rowCount !== 1) return res.status(500).json({});

		var event_query = 'INSERT INTO event(id, type, actor_id, repo_id, created_at) VALUES($1, $2, $3, $4, $5) RETURNING *';
		var event_values = [req.body.id, req.body.type, create_actor.rows[0].id, create_repo.rows[0].id, created_at];

		var create_event = await db.query(event_query, event_values);
		if(create_event.rowCount !== 1) return res.status(500).json({message: "Unable to create a event."});

		return res.status(201).json({});
		
	} catch (error) {
		return res.status(500).json({message: error.message});
	}
};

var getByActor = async (req, res) => {
	try {

		let query = 'SELECT * FROM event evt INNER JOIN actor act ON act.id = evt.actor_id INNER JOIN repo rep ON rep.id = evt.repo_id WHERE evt.actor_id = $1';
    	let value = [req.params.actorID];

		let events = await db.query(query, value);
		if(!events.rowCount) return res.status(404).json({});

		events = utilities.formatJsonData(events.rows);

		return res.status(200).json(events);

	} catch (error) {
		return res.status(500).json({message: error.message});
	}
};

var eraseEvents = async (req, res) => {
	var event_query = 'DELETE FROM event';
	var actor_query = 'DELETE FROM actor';
	var repo_query = 'DELETE FROM repo';

	try {
		await db.query(event_query);
		await db.query(actor_query);
		await db.query(repo_query);

		return res.status(200).json({});

	} catch (error) {
		return res.status(500).json({message: error.message});
	}
};

module.exports = {
	getAllEvents: getAllEvents,
	addEvent: addEvent,
	getByActor: getByActor,
	eraseEvents: eraseEvents
}